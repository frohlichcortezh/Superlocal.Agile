# Superlocal.Agile

Helper for devs on an agile team.

## Features

- Integration with
  - Redmine
  - Gitlab

### Windows only

- Tracks your time in different apps and let you sync with project management tools