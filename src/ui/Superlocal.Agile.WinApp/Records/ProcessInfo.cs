﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Superlocal.Agile.WinApp.Records
{
    internal interface IProcessInfo
    {
        int ProcessId { get; }

        string? ProcessName { get; }

        string? ProcessPath { get; }
    }

    internal record ProcessInfo(int ProcessId, string? ProcessName, string? ProcessPath) : IProcessInfo;

    internal static class ProcessInfoHelper
    {
        public static ProcessInfo? GetProcessInfo(int processId)
        {
            Process process = Process.GetProcessById(processId);
            
            return new ProcessInfo(processId, process?.ProcessName, process?.MainModule?.FileName);
        }
    }
}
