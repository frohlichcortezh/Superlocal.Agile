﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Superlocal.Agile.WinApp.Records
{
    internal record TimePeriod(DateTimeOffset Start)
    {
        public DateTimeOffset Stop { get; set; } = DateTime.MaxValue;
    }

    internal static class TimePeriodExtensions
    {
        public static bool AnyUnstopped(this List<TimePeriod> timePeriods) => timePeriods!= null && timePeriods.Any(t => t.Stop == DateTime.MaxValue);

        public static TimePeriod? GetUnstopped(this List<TimePeriod> timePeriods) => timePeriods.FirstOrDefault(t => t.Stop == DateTime.MaxValue);

        public static void Stop(this List<TimePeriod> timePeriods) => timePeriods.ForEach(t => { t.Stop = t.Stop == DateTime.MaxValue ? DateTime.Now : t.Stop; });
    }
}
