using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Superlocal.Agile.App.Services;
using Superlocal.Agile.Infrastructure.Database;
using Superlocal.Agile.WinApp.Models;

namespace Superlocal.Agile.WinApp
{
    internal static class Program
    {        

        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static async Task Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            ApplicationConfiguration.Initialize();

            IConfiguration config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables()
                .Build();

            var settings = config.GetRequiredSection("Settings").Get<Settings>();

            var dbPath = settings.DbPath;
            if (string.IsNullOrWhiteSpace(Path.GetDirectoryName(dbPath)))
            {
                dbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Superlocal.Agile", dbPath);
            }
            var connectionString = string.Format("Data Source={0};", dbPath);

            var builder = new HostBuilder()
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddSingleton(settings)                            
                            .AddDbContext<ApplicationDbContext>(options => options.UseSqlite(connectionString))
                            .AddSingleton<ActiveAppWatcher>()
                            .AddSingleton<MainForm>()
                            ;
                });

            var host = builder.Build();

            using (var serviceScope = host.Services.CreateScope())
            {
                var services = serviceScope.ServiceProvider;
                try
                {
                    var mainForm = services.GetRequiredService<MainForm>();
                    Application.Run(mainForm);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error Occured", ex.Message);
                }
            }
        }
    }
}