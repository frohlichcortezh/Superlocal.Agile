﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Superlocal.Agile.WinApp.Constants
{  
    internal enum ApplicationState
    {
        NotRunning,
        Running,
        Focused
    }
}
