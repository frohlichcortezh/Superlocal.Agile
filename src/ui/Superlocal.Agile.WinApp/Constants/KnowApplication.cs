﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Superlocal.Agile.WinApp.Constants
{
    /// <summary>
    /// List of known/treated applications
    /// </summary>
    internal enum KnowApplication
    {
        VisualStudio,
        VSCode,
        Firefox,
    }

    internal static class KnowApplicationExtensions
    {
        /// <summary>
        /// Return a know application by it's process name
        /// ToDo migth need to use processPath in case of unbiquity
        /// </summary>
        /// <param name="processName"></param>
        /// <param name="processPath"></param>
        /// <returns></returns>
        public static KnowApplication? ProcessToKnowApplication(string processName, string processPath = "")
        {
            switch (processName)
            {
                case "devenv":
                    return KnowApplication.VisualStudio;
                case "Code":
                    return KnowApplication.VSCode;
                case "firefox":
                    return KnowApplication.Firefox;
                default:
                    return null;
            }

        } 
    }
}
