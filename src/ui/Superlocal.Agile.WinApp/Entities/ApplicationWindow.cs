﻿using Superlocal.Agile.WinApp.Records;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Superlocal.Agile.WinApp.Entities
{
    internal class ApplicationWindow
    {
        public List<TimePeriod> TimePeriods { get; internal set; }

        public WindowInfo WindowInfo { get; internal set; }

        public int Id => WindowInfo.GetHashCode();

        public ApplicationWindow(WindowInfo windowInfo, bool start = false)
        {
            WindowInfo = windowInfo;
            TimePeriods = new List<TimePeriod>(); //ToDo get from repository
            if (start)
                StartWindowsActivation();
        }

        public void StopRunningWindow()
        {
            TimePeriods.Stop();
        }

        public void StartWindowsActivation()
        {
            StopRunningWindow();
            TimePeriods.Add(new TimePeriod(DateTime.Now));
        }

        public bool HasRunningWindow() => TimePeriods.AnyUnstopped();
    }

    internal static class ApplicationWindowExtensions
    {
        public static List<ApplicationWindow> StopRunningWindow(this List<ApplicationWindow> applicationWindows)
        {

            foreach (var application in applicationWindows)
            {
                if (application.HasRunningWindow())
                    application.StopRunningWindow();
            }

            return applicationWindows;
        }
    }
}
