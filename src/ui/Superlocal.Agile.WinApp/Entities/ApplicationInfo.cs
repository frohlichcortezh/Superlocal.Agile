﻿using Superlocal.Agile.WinApp.Constants;
using Superlocal.Agile.WinApp.Records;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Superlocal.Agile.WinApp.Entities
{
    
    internal class ApplicationInfo
    {
        public ApplicationState State { get; internal set; }

        public string? FullPath { get; internal set; }

        public int Id { get { return string.IsNullOrEmpty(FullPath) ? Guid.NewGuid().GetHashCode() : FullPath.GetHashCode(); } }

        public List<ApplicationWindow> Windows { get; internal set; }

        public ApplicationInfo(WindowInfo windowInfo, bool start = false)
        {
            FullPath = windowInfo.ProcessInfo?.ProcessPath;

            Windows = new List<ApplicationWindow>();
            Windows.Add(new ApplicationWindow(windowInfo, start));
        }

        public bool HasWindow(WindowInfo windowInfo) => Windows.Any(w => w.GetHashCode() == windowInfo.GetHashCode());

        public ApplicationWindow? GetApplicationWindow(WindowInfo windowInfo) => Windows.FirstOrDefault(w => w.GetHashCode() == windowInfo.GetHashCode());

        public bool HasRunningWindow() => Windows.Any(w => w.HasRunningWindow());

        public void StopRunningWindow()
        {
            Windows = Windows.StopRunningWindow();
        }

        public void AddWindow(WindowInfo windowInfo) => Windows.Add(new ApplicationWindow(windowInfo, true));
    }

    internal static class ApplicationInfoExtensions
    {
        public static List<ApplicationInfo> StopRunningWindow(this List<ApplicationInfo> applications)
        {
            
            foreach (var application in applications)
            {
                if (application.HasRunningWindow())
                    application.StopRunningWindow();
            }

            return applications;
        }

        public static List<ApplicationInfo> StartWindowActivation(this List<ApplicationInfo> applications, WindowInfo windowInfo)
        {
            if (applications == null)
                applications = new List<ApplicationInfo>();

#pragma warning disable CS8602 // Dereference of a possibly null reference.
            var application = applications.FirstOrDefault(a => a.FullPath == windowInfo.ProcessInfo.ProcessPath);
#pragma warning restore CS8602 // Dereference of a possibly null reference.
            if (application == null)
                applications.Add(new ApplicationInfo(windowInfo, true));
            else
            {
                var w = application.Windows.FirstOrDefault(w => w.WindowInfo.GetHashCode() == windowInfo.GetHashCode());
                if (w == null)
                    application.AddWindow(windowInfo);
                else
                    w.StartWindowsActivation();

            }

            return applications;
        }
    }
}
