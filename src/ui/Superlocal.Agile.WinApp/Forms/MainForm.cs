using Newtonsoft.Json;
using Superlocal.Agile.WinApp.Records;
using Superlocal.Agile.WinApp.Entities;
using MaterialSkin.Controls;
using MaterialSkin;
using Microsoft.Extensions.Logging;
using Superlocal.Agile.Infrastructure.Database;
using Superlocal.Agile.App.Services;

namespace Superlocal.Agile.WinApp
{
    public partial class MainForm : MaterialForm
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _dbContext;
        private readonly ActiveAppWatcher _activeAppWatcher;

        private WindowInfo? _activeWindowsInfo;
        private readonly SynchronizationContext? synchronizationContext;

        private Dictionary<string, List<TimePeriod>> timeSpent = new Dictionary<string, List<TimePeriod>>();
        private List<ApplicationInfo> Applications = new List<ApplicationInfo>();
        private List<WindowInfo> WindowsInfo = new List<WindowInfo>();

        public MainForm(ILogger<MainForm> logger, ApplicationDbContext dbContext, ActiveAppWatcher activeAppWatcher)
        {
            InitializeComponent();
            _logger = logger;
            _dbContext = dbContext;
            _activeAppWatcher = activeAppWatcher;
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            synchronizationContext = SynchronizationContext.Current; //context from UI thread  
            _activeAppWatcher.RunWorkerAsync();
            Task.Run(() =>
            {
                while (true)
                {
                    //GetActiveWindow();
                    WatchActiveWindow();
                }
            });
        }
     
        private void WatchActiveWindow() 
        {
            var windowInfo = WindowInfoHelper.GetActiveWindowInfo();

            if (windowInfo == null || (_activeWindowsInfo != null && windowInfo.GetHashCode() == _activeWindowsInfo.GetHashCode()))
                return;

            if (!WindowsInfo.Contains(windowInfo))
                WindowsInfo.Add(windowInfo);

            _logger.LogDebug(JsonConvert.SerializeObject(WindowsInfo));

            StopRunningWindow();
            StartWindowActivation(windowInfo);

            if (synchronizationContext == null)
                return;

            synchronizationContext.Post(new SendOrPostCallback(o =>
            {
                listBox2.Items.Add(JsonConvert.SerializeObject(windowInfo));
                textBox1.Text = o != null ? o.ToString() : "Untitled";

                treeView1.Nodes.Clear();
                foreach (var app in Applications)
                {
                    var p = treeView1.Nodes.Add(app.FullPath);
                    foreach (var window in app.Windows)
                    {
                        var w = p.Nodes.Add(window.WindowInfo.Title);
                        foreach (var time in window.TimePeriods)
                        {
                            w.Nodes.Add(time.Start.ToString() + " - " + time.Stop.ToString() + ": " + time.Stop.Subtract(time.Start).TotalMinutes);
                        }
                    }
                }
                treeView1.ExpandAll();
            }), windowInfo.Title);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            File.WriteAllText(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Applications.json"), JsonConvert.SerializeObject(Applications));
            File.WriteAllText(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "WindowsInfo.json"), JsonConvert.SerializeObject(WindowsInfo));
        }

        public void StopRunningWindow()
        {
            this.Applications = this.Applications.StopRunningWindow();
        }

        private void StartWindowActivation(WindowInfo windowInfo)
        {
            _activeWindowsInfo = windowInfo;
            this.Applications = this.Applications.StartWindowActivation(_activeWindowsInfo);
        }

    }    
}