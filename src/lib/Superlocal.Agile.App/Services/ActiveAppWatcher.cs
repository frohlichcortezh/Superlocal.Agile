﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Superlocal.Agile.Infrastructure.Database;
using Superlocal.Agile.Infrastructure.Entities;
using System.ComponentModel;
using System.Diagnostics;

namespace Superlocal.Agile.App.Services
{
    public class ActiveAppWatcher : BackgroundWorker
    {
        private IntPtr _activeWindowHandle;
        private string _activeWindowTitle;
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _dbContext;

        public ActiveAppWatcher(ILogger<ActiveAppWatcher> logger, ApplicationDbContext dbContext)
        {
            _logger = logger;
            _dbContext = dbContext;
            this.WorkerSupportsCancellation = true;
        }
        

        protected override void OnDoWork(DoWorkEventArgs e)
        {
            while (!this.CancellationPending)
            {
                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                WatchActiveWindow();
            }
        }

        private async void WatchActiveWindow()
        {
            var activeWindowHandle = PInvoke.User32.GetForegroundWindow();
            var windowTitle = string.Empty;

            try
            {
                windowTitle = PInvoke.User32.GetWindowText(_activeWindowHandle);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error while getting window title.");
            }
    
            if (activeWindowHandle == IntPtr.Zero || (activeWindowHandle == _activeWindowHandle && windowTitle == _activeWindowTitle ))
                return;

            var switchedAppAt = DateTimeOffset.Now;

            _activeWindowHandle = activeWindowHandle;

            int processId;
            PInvoke.User32.GetWindowThreadProcessId(_activeWindowHandle, out processId);
            Process process = Process.GetProcessById(processId);

            ProcessInfo processInfo = null;
            if (process != null)
            {
                if (string.IsNullOrEmpty(windowTitle))
                {
                    windowTitle = process.ProcessName;
                }

                // ToDo check if process doesn't have name
                // If it doesn't have try to get it from windows title
                processInfo = _dbContext.ProcessInfos.FirstOrDefault(p => p.Name == process.ProcessName);
                if (processInfo == null)
                {
                    processInfo = new ProcessInfo();
                    processInfo.Path = process.MainModule?.FileName;
                    processInfo.Name = process.ProcessName;
                    processInfo.LastCapturedId = process.Id;
                    _dbContext.Add(processInfo);
                }
            }

            if (string.IsNullOrEmpty(windowTitle))
            {
                windowTitle = "Unknown";
            }

            _activeWindowTitle = windowTitle;

            AppWindowInfo appWindowInfo = _dbContext.AppWindowInfos.FirstOrDefault(a => a.End == DateTimeOffset.MaxValue);
            if (appWindowInfo != null)
            {
                appWindowInfo.End = switchedAppAt;
            }

            AppWindowInfo activeAppWindowInfo = new AppWindowInfo()
            {
                Title = windowTitle,
                Start = switchedAppAt,
                End = DateTimeOffset.MaxValue,
                ProcessInfo = processInfo,
            };
            _dbContext.Add(activeAppWindowInfo);

            await _dbContext.SaveChangesAsync();
        }
    }
}