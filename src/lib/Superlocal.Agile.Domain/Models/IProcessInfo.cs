﻿namespace Superlocal.Agile.Domain.Models;

public partial interface IProcessInfo
{
    int LastCapturedId { get; }

    string Name { get; }

    string? Path { get; }
}