﻿namespace Superlocal.Agile.Domain.Models
{
    public interface IAppWindowInfo
    {
        /// <summary>
        /// App window title
        /// </summary>
        string Title { get; }

        /// <summary>
        /// When user interacted with app window
        /// </summary>
        DateTimeOffset Start { get; }

        /// <summary>
        /// When user moved to another app
        /// </summary>
        DateTimeOffset End { get; } 
    }
}
