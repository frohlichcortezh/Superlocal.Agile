﻿# Domain

Add info about DDD and Clean Archictecture

## Model
 
People often confuse entity with model. However, these two are quite different. 

A model typically represents a real world object that is related to the problem or domain space. 
While programming, we create classes to represent them. These classes, known as models, 
have some properties and methods (defining their behavior) in a particular domain space. 
For instance, in any customer oriented problem, we may have a customer class that has some properties and methods.

In some ORM (Object Relational Mapper) frameworks, a model is tightly bound to an entity. 
This means that every scalar property maps to an entity attribute. 
However, if you are familiar with Entity Framework, you must know that, not all properties in your domain class map to an entity column. 
And that's one way in which an entity is different from a model.

### References

https://www.c-sharpcorner.com/blogs/entity-vs-model-vs-viewmodel-vs-datamodel
