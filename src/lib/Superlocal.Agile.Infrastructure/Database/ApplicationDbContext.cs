﻿using Microsoft.EntityFrameworkCore;
using Superlocal.Agile.Infrastructure.Entities;

namespace Superlocal.Agile.Infrastructure.Database
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<ProcessInfo> ProcessInfos { get; set; }

        public DbSet<AppWindowInfo> AppWindowInfos { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        //protected override void OnConfiguring(DbContextOptionsBuilder options)
        //{
        //    base.OnConfiguring(options);
        //}
    }
}
