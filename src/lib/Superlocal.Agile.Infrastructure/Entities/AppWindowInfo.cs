﻿using System.ComponentModel.DataAnnotations;

namespace Superlocal.Agile.Infrastructure.Entities;

public class AppWindowInfo : IAppWindowInfo
{
    [Key]
    public int Id { get; set; }

    public ProcessInfo ProcessInfo { get; set; }

    [Required]
    public string Title { get; set; }

    [Required]
    public DateTimeOffset Start { get; set; }

    [Required]
    public DateTimeOffset End { get; set; }
}
