﻿using System.ComponentModel.DataAnnotations;

namespace Superlocal.Agile.Infrastructure.Entities;

public class ProcessInfo : IProcessInfo
{
    [Key]
    public Guid Guid { get; set; }

    public int LastCapturedId { get; set; }

    [Required]
    public string Name { get; set; }

    public string? Path { get; set; }

    public virtual IEnumerable<AppWindowInfo> AppWindowsInfo { get; set; }
}
