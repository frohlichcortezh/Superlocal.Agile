﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Superlocal.Agile.Infrastructure.Migrations
{
    public partial class Firstmigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProcessInfos",
                columns: table => new
                {
                    Guid = table.Column<Guid>(type: "TEXT", nullable: false),
                    LastCapturedId = table.Column<int>(type: "INTEGER", nullable: false),
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    Path = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProcessInfos", x => x.Guid);
                });

            migrationBuilder.CreateTable(
                name: "AppWindowInfos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ProcessInfoGuid = table.Column<Guid>(type: "TEXT", nullable: false),
                    Title = table.Column<string>(type: "TEXT", nullable: false),
                    Start = table.Column<DateTimeOffset>(type: "TEXT", nullable: false),
                    End = table.Column<DateTimeOffset>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppWindowInfos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppWindowInfos_ProcessInfos_ProcessInfoGuid",
                        column: x => x.ProcessInfoGuid,
                        principalTable: "ProcessInfos",
                        principalColumn: "Guid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AppWindowInfos_ProcessInfoGuid",
                table: "AppWindowInfos",
                column: "ProcessInfoGuid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AppWindowInfos");

            migrationBuilder.DropTable(
                name: "ProcessInfos");
        }
    }
}
